#include "advert.h"

Advert::Advert()
{
	this->date = new Date();
	this->phone = new char[12];
	this->category = new Category();
	this->text = new char[1];
}

Advert::Advert(Date date, const char phone[11], Category category, const char* text)
{
	this->date = new Date();
	this->phone = new char[12];
	this->category = new Category();

	setDate(date);
	setCategory(category);
	setPhone(phone);
	setText(text);
}

Advert::Advert(Advert& advert)
{
	this->date = new Date();
	this->phone = new char[12];
	this->category = new Category();

	setDate(*advert.getDate());
	setCategory(*advert.getCategory());
	setPhone(advert.getPhone());
	setText(advert.getText());
}

Advert::~Advert()
{
	delete this->date;
	delete[] this->phone;
	delete this->category;
	delete[] this->text;
}

Advert* Advert::setDate(Date date)
{
	*this->date = date;
	return this;
}

Advert* Advert::setCategory(Category category)
{
	*this->category = category;
	return this;
}

Advert* Advert::setPhone(const char phone[11])
{
	strcpy(this->phone, phone);
	return this;
}

Advert* Advert::setText(const char* text)
{
	this->text = new char[strlen(text) + 1];
	strcpy(this->text, text);
	return this;
}

Advert* Advert::binWrite(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// ������ ����� �������
	this->date->binWrite(stream);

	// ������ ���������� ����
	int length = strlen(this->phone);				// ���������� ����� ������
	stream.write((char*)&length, sizeof(length));	// ������ ����� ������
	stream.write(this->phone, length);				// ������ ������

	int catId = categoryToInt(this->category);
	stream.write((char*)&catId, sizeof(catId));

	length = strlen(this->text);
	stream.write((char*)&length, sizeof(length));
	stream.write(this->text, length);

	return this;
}

Advert* Advert::binRead(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// ������ ����� �������
	this->date->binRead(stream);

	int length = 0;

	// ������ ���������� ����
	delete[] this->phone;							// ������� ������
	stream.read((char*)&length, sizeof(length));	// ������ ����� ������
	this->phone = new char[length + 1];				// ��������� ������ ��� ������
	stream.read(this->phone, length);				// ������ ������
	this->phone[length] = '\0';						// ���������� ������� ����� ������

	int catId = 0;
	stream.read((char*)&catId, sizeof(catId));
	this->setCategory(toCategory(catId));

	delete[] this->text;
	stream.read((char*)&length, sizeof(length));
	this->text = new char[length + 1];
	stream.read(this->text, length);
	this->text[length] = '\0';

	return this;
}

std::ostream& operator<<(std::ostream& stream, Advert& advert)
{
	stream
		<< "Date: " << *advert.date
		<< "\nPhone: +" << advert.getPhone()
		<< "\nCategory: " << categoryToStr(advert.getCategory())
		<< "\nText: " << advert.getText() << "\n";

	return stream;
}

std::istream& Advert::operator>>(std::istream& stream)
{
	Date date;
	stream >> date;
	this->setDate(date);

	char phone[12];
	std::cout << "Insert phone: ";
	stream >> phone;
	this->setPhone(phone);

	int cat;
	std::cout << "Insert category: 1)Sell, Buy 2)Transport 3)Property 4)Materials 0)None: ";
	stream >> cat;
	this->setCategory(toCategory(cat));

	char text[255];
	std::cout << "Insert text: ";
	stream.ignore();
	stream.getline(text, sizeof(text));
	this->setText(text);

	return stream;
}

bool operator<(Advert a1, Advert a2)
{
	return *a1.getDate() < *a2.getDate();
}

bool operator>(Advert a1, Advert a2)
{
	return *a1.getDate() > *a2.getDate();
}

bool operator==(Advert a1, Advert a2)
{
	return *a1.getDate() == *a2.getDate();
}
