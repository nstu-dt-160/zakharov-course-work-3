#include <iostream>
#include "category.h"
#include "error.h"

const char* categoryToStr(Category *category)
{
	switch (*category)
	{
	case SELL_BUY:	return "Sell, Buy";
	case TRANSPORT: return "Transport";
	case PROPERTY:	return "Property";
	case MATERIALS: return "Materials";
	case NONE:		return "None";

	default:		throw Error("invalid category", -2);
	}
}

int categoryToInt(Category* category)
{
	return static_cast<int>(*category);
}

Category toCategory(int number)
{
	return static_cast<Category>(number);
}