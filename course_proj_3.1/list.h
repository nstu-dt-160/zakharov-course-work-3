#pragma once
#include <cstring>
#include <iostream>
#include <fstream>
#include "error.h"
#include "category.h"
#include "advert.h"

template<typename T> class List
{
private:
	struct Node {
		T* value;
		Node* prev, * next;

		Node(T* data);
	};

	int size;
	Node* head, * tail;

public:
	List();
	~List();

	List<T>* pushFront(T* data);		
	List<T>* pushBack(T* data);
	List<T>* push(T* data, int index);

	T* popFront();
	T* popBack();
	T* pop(int index);

	Node* getNode(int index);	// ���������� �������
	T* operator[](int index);	// ���������� �������� ��������

	List<T>* sort();
	void swap(int index1, int index2);		// ������ �������� ������ �������

	List<T>* search(Category category);		// ���������� ����� ������ ��������� �� ���������
	List<T>* search(const char* str);		// ���������� ����� ������ ��������� �� ���������

	List<T>* empty();	// ������� ������

	List<T>* binWrite(std::fstream& stream);	// ������ ������ � �������� ����
	List<T>* binRead(std::fstream& stream);		// ������ ������ �� ��������� �����

	int getSize() { return this->size; }
	bool isEmpty() { return this->size == 0; }

	List<T>* print();	// ����� ������ �� �����
};

template<typename T> void List<T>::swap(int index1, int index2)
{
	Node* el1 = this->getNode(index1),
		* el2 = this->getNode(index2);

	// ������������ ��������
	T* tmp = el1->value;
	el1->value = el2->value;
	el2->value = tmp;
}

template<typename T> List<T>* List<T>::search(Category category)
{
	List<T>* list = new List<T>();
	Node* node = this->head;

	for (int i = 0; i < this->size; i++) {
		Advert adv = *node->value;
		if (*adv.getCategory() == category) {
			list->pushBack(node->value);
		}

		node = node->next;
	}

	return list;
}

template<typename T> List<T>* List<T>::search(const char* str)
{
	List<T>* list = new List<T>();
	Node* node = this->head;

	for (int i = 0; i < this->size; i++) {
		Advert adv = *node->value;
		if (strstr(adv.getText(), str) != NULL) {
			list->pushBack(node->value);
		}

		node = node->next;
	}

	return list;
}

template<typename T> List<T>::Node::Node(T* value)
{
	this->value = value;
	this->next = this->prev = nullptr;
}

template<typename T> List<T>::List()
{
	this->size = 0;
	this->head = this->tail = nullptr;
}

template<typename T> List<T>::~List()
{
	this->empty();
}

template<typename T> List<T>* List<T>::empty()
{
	for (int i = 0; i < size; i++) {
		Node* node = this->head;
		this->head = this->head->next;
		delete node;
	}

	this->size = 0;

	return this;
}

template<typename T> List<T>* List<T>::pushFront(T* data)
{
	Node* node = new Node(data);

	if (size == 0) head = tail = node;	// ������� � ������ ������
	else {
		node->next = this->head;
		this->head->prev = node;

		this->tail->next = node;
		node->prev = this->tail;

		this->head = node;
	}

	size++;

	return this;
}

template<typename T> List<T>* List<T>::pushBack(T* data)
{
	Node* node = new Node(data);

	if (size == 0) head = tail = node;	// ������� � ������ ������
	else {
		node->prev = this->tail;
		this->tail->next = node;

		node->next = this->head;
		this->head->prev = node;

		this->tail = node;
	}

	size++;

	return this;
}

template<typename T>
inline List<T>* List<T>::push(T* data, int index)
{
	if (index < 0 || index > this->size) {
		throw Error("Invalid index", -4);
	}

	if (index == 0) {
		return this->pushFront(data);
	}
	else if (index == this->size) {
		return this->pushBack(data);
	}

	Node* node = this->getNode(index);	// ���������� �������
	Node* newNode = new Node(data);

	// ������� � �������� ������
	newNode->next = node;
	newNode->prev = node->prev;
	node->prev->next = newNode;
	node->prev = newNode;

	this->size++;

	return this;
}

template<typename T> T* List<T>::popFront()
{
	if (this->isEmpty()) {
		throw Error("List is empty", -1);
	}

	Node* node = this->head;

	this->head = node->next;
	this->head->prev = node->prev;
	node->prev->next = this->head;

	this->size--;

	T* value = new T(*node->value);	// �������� ����������
	delete node;

	return value;
}

template<typename T> T* List<T>::popBack()
{
	if (this->isEmpty()) {
		throw Error("List is empty", -1);
	}

	Node* node = this->tail;

	this->tail = node->prev;
	this->tail->next = node->next;
	node->next->prev = this->tail;

	this->size--;

	T* value = new T(*node->value);	// �������� ����������
	delete node;

	return value;
}

template<typename T>
inline T* List<T>::pop(int index)
{
	if (index < 0 || index >= this->size) {
		throw Error("Invalid index", -4);
	}

	if (index == 0) {
		return this->popFront();
	}
	else if (index == this->size - 1) {
		return this->popBack();
	}

	Node* node = this->getNode(index);
	T* value = new T(*node->value); // �������� ����������

	node->next->prev = node->prev;
	node->prev->next = node->next;
	
	delete node;
	this->size--;

	return value;
}

template<typename T> List<T>* List<T>::print()
{
	for (int i = 0; i < this->size; i++) {
		std::cout << *this->operator[](i) << '\n';
	}
	return this;
}

template<typename T> inline List<T>* List<T>::sort()
{
	// ���������� ���������
	for (int i = 1; i < this->size; i++) {
		for (int j = i; j > 0 && *this->operator[](j - 1) > *this->operator[](j); j--) {
			this->swap(j - 1, j);
		}
	}

	return this;
}

template<typename T> List<T>* List<T>::binWrite(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// ������ ���������� ��������� ������
	stream.write((char*)&this->size, sizeof(this->size));

	Advert* adv;

	// ������ ��������� ������
	for (int i = 0; i < this->size; i++) {
		adv = this->operator[](i);
		adv->binWrite(stream);
	}

	return this;
}

template<typename T> List<T>* List<T>::binRead(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// ������ ���������� ��������� ������
	int size = 0;
	stream.read((char*)&size, sizeof(size));

	// ������ ��������� ������
	for (int i = 0; i < size; i++) {
		Advert* adv = new Advert();
		adv->binRead(stream);
		this->pushBack(adv);
	}

	return this;
}

template<typename T> typename List<T>::Node* List<T>::getNode(int index)
{
	if (index < 0 || index >= this->size) {
		throw Error("Invalid index", -4);
	}

	Node* node = this->head;

	for (int i = 0; i < index; i++) {
		node = node->next;
	}

	return node;
}

template<typename T> T* List<T>::operator[](int index)
{
	return this->getNode(index)->value;
}