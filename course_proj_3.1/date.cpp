#include "date.h"
#include <iostream>
#include <fstream>
#include <string>
#include "error.h"

bool G_DEBUG = false;

Date::Date()
{
	this->year = 2000;
	this->month = 1;
	this->day = 1;
}

Date::Date(int year, int month, int day)
{
	this->setDate(year, month, day);
}

Date::Date(const Date& date)
{
	this->setDate(date.year, date.month, date.day);
}

int Date::getDaysInMonth(int year, int month)
{
	// ������ ���������� ���� �� �������
	const int _days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	int daysCount = _days[month - 1];
	// ����������� ��� ������� ����������� ����
	if (month == 2 && isLeapYear(year)) {
		daysCount++;
	}

	return daysCount;
}

bool Date::isLeapYear(int year)
{
	if (year % 4 != 0 || year % 100 == 0 && year % 400 != 0) {
		return false;
	}

	return true;
}

Date* Date::setDate(int year, int month, int day)
{
	validateDate(year, month, day);

	this->year = year;
	this->month = month;
	this->day = day;

	return this;
}

Date* Date::setYear(int year)
{
	validateDate(year, this->month, this->day);
	this->year = year;

	return this;
}

Date* Date::setMonth(int month)
{
	validateDate(this->year, month, this->day);
	this->month = month;

	return this;
}

Date* Date::setDay(int day)
{
	validateDate(this->year, this->month, day);
	this->day = day;

	return this;
}

void Date::validateDate(int year, int month, int day)
{
	if (year < 0 || year > 2100) throw Error("incorrect year", -1);
	if (month < 0 || month > 12) throw Error("incorrect month", -1);
	if (day < 0 || day > getDaysInMonth(year, month)) throw Error("incorrect day", -1);
}

void Date::operator=(Date date)
{
	this->setDate(date.year, date.month, date.day);
}

bool operator==(Date& date1, Date& date2)
{
	if (date1.getYear() == date2.getYear() &&
		date1.getMonth() == date2.getMonth() &&
		date1.getDay() == date2.getDay()) {
		return true;
	}

	return false;
}

bool operator<(Date& date1, Date& date2)
{
	if (date1.getYear() < date2.getYear()) return true;
	if (date1.getYear() > date2.getYear()) return false;

	if (date1.getMonth() < date2.getMonth()) return true;
	if (date1.getMonth() > date2.getMonth()) return false;

	if (date1.getDay() < date2.getDay()) return true;
	if (date1.getDay() > date2.getDay()) return false;

	return false;
}

bool operator>(Date& date1, Date& date2)
{
	if (date1.getYear() > date2.getYear()) return true;
	if (date1.getYear() < date2.getYear()) return false;

	if (date1.getMonth() > date2.getMonth()) return true;
	if (date1.getMonth() < date2.getMonth()) return false;

	if (date1.getDay() > date2.getDay()) return true;
	if (date1.getDay() < date2.getDay()) return false;

	return false;
}

std::ostream& operator<<(std::ostream& stream, Date& date)
{
	stream << std::to_string(date.year) << '.';
	if (date.month < 10) stream << '0';
	stream  << std::to_string(date.month) << '.';
	if (date.day < 10) stream << '0';
	stream << std::to_string(date.day);

	return stream;
}

std::istream& operator>>(std::istream& stream, Date& date)
{
	int y, m, d;

	std::cout << "Insert date (yyyy mm dd): ";
	stream >> y >> m >> d;

	date.setDate(y, m, d);
	return stream;
}

Date* Date::binWrite(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// ������ ����� � ����������� ����
	stream.write((char*)&this->year, sizeof(this->year));
	stream.write((char*)&this->month, sizeof(this->month));
	stream.write((char*)&this->day, sizeof(this->day));

	return this;
}

Date* Date::binRead(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// ������ ����� � ����������� ����
	stream.read((char*)&this->year, sizeof(this->year));
	stream.read((char*)&this->month, sizeof(this->month));
	stream.read((char*)&this->day, sizeof(this->day));

	return this;
}
