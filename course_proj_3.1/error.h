#pragma once
#include <string>

// ����� ������ ��������� ���������
class Error
{
private:
	std::string message;
	int code;

public:
	Error(std::string message, int code) {
		this->message = message;
		this->code = code;
	}

	std::string getMessage() { return message; }
	int getCode() { return code; }
};

