#pragma once
#include <iostream>
#include <cstdlib>
#include "list.h"
#include "advert.h"
#include "error.h"
#include "date.h"

#ifdef _WIN32
#define CLEAR "cls"
#else //In any other OS
#define CLEAR "clear"
#endif

class Application
{
private:
	List<Advert>* list;

public:
	Application();
	~Application();

	void launch();			// ������ ���������
	void test(int count);	// �������������� ������������

private:
	void printList();		// ����� ������ �� �����
	
	void add();				// ���������� ��������
	void addFirst();
	void addLast();
	void addN();

	void remove();			// �������� ��������
	void removeFirst();
	void removeLast();
	void removeN();
	void removeAll();

	void sort();			// ����������

	void file();			// ������ � �������� ������
	void fileSave();
	void fileLoad();

	void search();			// ����� �� ������
	void searchCat();
	void searchText();
};

