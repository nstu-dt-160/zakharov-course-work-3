﻿#include "application.h"

int main()
{
    try {
        Application app = Application();
        app.launch();
    }
    catch (Error& e) {
        std::cout << "[ERROR]: " << e.getMessage() << '\n';
        return e.getCode();
    }

    return 0;
}