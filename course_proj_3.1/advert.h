#pragma once
#include <ctime>
#include <iostream>
#include <fstream>
#include <cstring>
#include "category.h"
#include "date.h"
#include "error.h"

class Advert
{
private:
	Date* date;
	char* phone;
	Category* category;
	char* text;

public:
	Advert();
	Advert(Date date, const char phone[11], Category category, const char* text);
	Advert(Advert& advert);

	~Advert();

	Date* getDate() { return this->date; }
	const char* getPhone() { return this->phone; }
	Category* getCategory() { return this->category; }
	const char* getText() { return this->text; }

	Advert* setDate(Date date);
	Advert* setCategory(Category category);
	Advert* setPhone(const char phone[11]);
	Advert* setText(const char* text);

	friend std::ostream& operator << (std::ostream& stream, Advert& advert);
	std::istream& operator >> (std::istream& stream);

	friend bool operator < (Advert a1, Advert a2);
	friend bool operator > (Advert a1, Advert a2);
	friend bool operator == (Advert a1, Advert a2);

	Advert* binWrite(std::fstream& stream);	// ������ ������� � �������� ����
	Advert* binRead(std::fstream& stream);	// ������ ������� �� ��������� �����
};